<?php

# shortlinks
remove_action( 'template_redirect', 'wp_shortlink_header', 11 );
remove_action('wp_head', 'wp_shortlink_wp_head');

# <link rel="EditURI" type="application/rsd xml" title="RSD" href="http://bhoover.com/wp/xmlrpc.php?rsd
remove_action ('wp_head', 'rsd_link');

# <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://bhoover.com/wp/wp-includes/wlwmanifest.xml">
remove_action( 'wp_head', 'wlwmanifest_link');

# <meta name="generator" content="WordPress 3.4.2">
remove_action('wp_head', 'wp_generator');

// disable emojicons
function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );